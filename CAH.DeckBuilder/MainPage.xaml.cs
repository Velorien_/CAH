﻿using CAH.DeckBuilder.Views;
using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace CAH.DeckBuilder
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void Navigation_SelectionChanged(NavigationView sender, NavigationViewSelectionChangedEventArgs args)
        {
            ContentFrame.Navigate(GetPageByTag((args.SelectedItem as NavigationViewItem).Tag as string));
        }

        private Type GetPageByTag(string tag)
        {
            switch (tag)
            {
                case "deckBrowser":
                    return typeof(DeckBrowserPage);
                case "deckBuilder":
                    return typeof(DeckBuilderPage);
                default:
                    throw new ArgumentOutOfRangeException("tag");
            }
        }

        private void Navigation_Loaded(object sender, RoutedEventArgs e)
        {
            Navigation.SelectedItem = Navigation.MenuItems.FirstOrDefault();
        }
    }
}
