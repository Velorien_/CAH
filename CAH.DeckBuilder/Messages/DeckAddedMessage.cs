﻿namespace CAH.DeckBuilder.Messages
{
    class DeckAddedMessage
    {
        public DeckAddedMessage(Deck deck) => Deck = deck;

        public Deck Deck { get; }
    }
}
