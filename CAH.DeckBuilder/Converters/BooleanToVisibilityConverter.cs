﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace CAH.DeckBuilder.Converters
{
    class BooleanToVisibilityConverter : IValueConverter
    {
        public bool IsInverted { get; set; }

        public object Convert(object value, Type targetType, object parameter, string language) =>
            (bool)value ^ IsInverted ? Visibility.Visible : Visibility.Collapsed;

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
