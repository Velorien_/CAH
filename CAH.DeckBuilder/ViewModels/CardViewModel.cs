﻿using GalaSoft.MvvmLight;

namespace CAH.DeckBuilder.ViewModels
{
    class CardViewModel : ObservableObject
    {
        private string text;
        public string Text
        {
            get => text;
            set
            {
                if (!string.IsNullOrWhiteSpace(value)) Set(ref text, value);
            }
        }

        private bool editMode;
        public bool EditMode
        {
            get => editMode;
            set => Set(ref editMode, value);
        }
    }

    class WhiteCardViewModel : CardViewModel { }

    class BlackCardViewModel : CardViewModel
    {
        private int pick;
        public int Pick
        {
            get => pick;
            set => Set(ref pick, value);
        }
    }
}
