﻿using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;
using System;
using System.Linq;
using CAH.DeckBuilder.Messages;
using GalaSoft.MvvmLight.Command;
using Microsoft.EntityFrameworkCore;
using CAH.DeckBuilder.Services;
using Refit;
using Windows.UI.Popups;
using Newtonsoft.Json.Linq;
using Windows.ApplicationModel.DataTransfer;

namespace CAH.DeckBuilder.ViewModels
{
    class DeckBrowserViewModel : ViewModelBase
    {
        private readonly DeckBuilderDbContext db;

        public DeckBrowserViewModel(DeckBuilderDbContext db)
        {
            this.db = db;
            MessengerInstance.Register<DeckAddedMessage>(this, m =>
            {
                if(Decks.Any(x => x.Name == m.Deck.Name))
                {
                    Decks.Remove(Decks.Single(x => x.Name == m.Deck.Name));
                }

                Decks.Add(m.Deck);
            });
            Initialize();
        }

        public ObservableCollection<Deck> Decks { get; } = new ObservableCollection<Deck>();

        private Deck selectedDeck;
        public Deck SelectedDeck
        {
            get => selectedDeck;
            set => UpdateDeck(value);
        }

        private string cahUrl;
        public string CahUrl
        {
            get => cahUrl;
            set
            {
                Set(ref cahUrl, value);
                FetchDecks.RaiseCanExecuteChanged();
                UploadDeck.RaiseCanExecuteChanged();
            }
        }

        private string importToken;
        public string ImportToken
        {
            get => importToken;
            set
            {
                Set(ref importToken, value);
                UploadDeck.RaiseCanExecuteChanged();
            }
        }

        public RelayCommand Delete { get; private set; }

        public RelayCommand DeleteAll { get; private set; }

        public RelayCommand FetchDecks { get; private set; }

        public RelayCommand UploadDeck { get; private set; }

        public RelayCommand CopyJson { get; private set; }

        private void UpdateDeck(Deck deck)
        {
            Set(() => SelectedDeck, ref selectedDeck, deck);
            Delete.RaiseCanExecuteChanged();
            UploadDeck.RaiseCanExecuteChanged();
            CopyJson.RaiseCanExecuteChanged();
        }

        private void Initialize()
        {
            var decks = db.Decks.Include(e => e.WhiteCards).Include(e => e.BlackCards);
            Decks.Clear();
            foreach (var item in decks) Decks.Add(item);

            Delete = new RelayCommand(async () =>
            {
                db.Decks.Remove(SelectedDeck);
                Decks.Remove(SelectedDeck);
                await db.SaveChangesAsync();
                MessengerInstance.Send(new DecksChangedMessage());
            }, () => SelectedDeck != null);

            DeleteAll = new RelayCommand(async () =>
            {
                Decks.Clear();
                db.Decks.RemoveRange(db.Decks);
                await db.SaveChangesAsync();
                MessengerInstance.Send(new DecksChangedMessage());
            }, () => Decks.Any());
            Decks.CollectionChanged += (s, a) => DeleteAll.RaiseCanExecuteChanged();

            FetchDecks = new RelayCommand(async () =>
            {
                try
                {
                    var result = await RestService.For<ICahDecksApi>(CahUrl).GetDecks();
                    foreach (var item in result)
                    {
                        if (!Decks.Any(x => x.Name == item.Name))
                        {
                            Decks.Add(item);
                            db.Decks.Add(item);
                        }
                    }

                    await db.SaveChangesAsync();
                    MessengerInstance.Send(new DecksChangedMessage());
                }
                catch (Exception ex)
                {
                    var md = new MessageDialog($"Could not get decks data.\n{ex.Message}", "Error");
                    await md.ShowAsync();
                }
            }, () => Uri.TryCreate(CahUrl, UriKind.Absolute, out var uri) && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps));

            UploadDeck = new RelayCommand(async () =>
            {
                var api = RestService.For<ICahDecksApi>(CahUrl);
                try
                {
                    await api.PostDeck(new
                    {
                        Name = SelectedDeck.Name,
                        WhiteCards = SelectedDeck.WhiteCards.Select(x => x.Text),
                        BlackCards = SelectedDeck.BlackCards.Select(x => new { Pick = x.Pick, Text = x.Text })
                    }, ImportToken);
                }
                catch (Exception ex)
                {
                    var md = new MessageDialog($"Could not send deck data.\n{ex.Message}", "Error");
                    await md.ShowAsync();
                }

            }, () => SelectedDeck != null && Uri.TryCreate(CahUrl, UriKind.Absolute, out var uri)
                && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps));

            CopyJson = new RelayCommand(() => {
                string json = JObject.FromObject(new
                {
                    Name = SelectedDeck.Name,
                    WhiteCards = SelectedDeck.WhiteCards.Select(x => x.Text),
                    BlackCards = SelectedDeck.BlackCards.Select(x => new { Pick = x.Pick, Text = x.Text })
                }).ToString();
                var dataPackage = new DataPackage { RequestedOperation = DataPackageOperation.Copy };
                dataPackage.SetText(json);
                Clipboard.SetContent(dataPackage);
            }, () => SelectedDeck != null);
        }
    }
}
