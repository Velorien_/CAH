﻿using CAH.DeckBuilder.Messages;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using Windows.UI.Popups;

namespace CAH.DeckBuilder.ViewModels
{
    class DeckBuilderViewModel : ViewModelBase
    {
        private readonly DeckBuilderDbContext db;

        public DeckBuilderViewModel(DeckBuilderDbContext db)
        {
            this.db = db;
            InitializeCommands();
            Initialize();
        }

        public ObservableCollection<WhiteCardViewModel> WhiteCards { get; } = new ObservableCollection<WhiteCardViewModel>();

        public ObservableCollection<BlackCardViewModel> BlackCards { get; } = new ObservableCollection<BlackCardViewModel>();

        public ObservableCollection<DeckComboBoxItemViewModel> Decks { get; } = new ObservableCollection<DeckComboBoxItemViewModel>();

        private WhiteCardViewModel selectedWhiteCard;
        public WhiteCardViewModel SelectedWhiteCard
        {
            get => selectedWhiteCard;
            set => Set(ref selectedWhiteCard, value);
        }

        private BlackCardViewModel selectedBlackCard;
        public BlackCardViewModel SelectedBlackCard
        {
            get => selectedBlackCard;
            set => Set(ref selectedBlackCard, value);
        }

        private string whiteCard;
        public string WhiteCard
        {
            get => whiteCard;
            set => Set(ref whiteCard, value);
        }

        private string blackCard;
        public string BlackCard
        {
            get => blackCard;
            set => Set(ref blackCard, value);
        }

        private int pick = 1;
        public int Pick
        {
            get => pick;
            set => Set(ref pick, value);
        }

        private string deckName;
        public string DeckName
        {
            get => deckName;
            set
            {
                Set(ref deckName, value);
                SaveDeck.RaiseCanExecuteChanged();
            }
        }

        private DeckComboBoxItemViewModel deckToLoad;
        public DeckComboBoxItemViewModel DeckToLoad
        {
            get => deckToLoad;
            set
            {
                Set(ref deckToLoad, value);
                LoadDeck.RaiseCanExecuteChanged();
            }
        }

        public RelayCommand AddWhiteCard { get; private set; }

        public RelayCommand AddBlackCard { get; private set; }

        public RelayCommand RemoveWhiteCard { get; set; }

        public RelayCommand RemoveBlackCard { get; private set; }

        public RelayCommand SaveDeck { get; private set; }
        public bool CanSaveDeck() => !string.IsNullOrEmpty(DeckName) && (WhiteCards.Any() || BlackCards.Any());

        public RelayCommand ClearDeck { get; private set; }

        public RelayCommand LoadDeck { get; private set; }

        private void Initialize()
        {
            LoadDecks();
            MessengerInstance.Register<DecksChangedMessage>(this, m => LoadDecks());
        }

        private void LoadDecks()
        {
            Decks.Clear();
            DeckToLoad = null;
            var deckNames = db.Decks.Select(e => new { e.Name, e.Id });
            foreach (var item in deckNames) Decks.Add(new DeckComboBoxItemViewModel { Id = item.Id, Name = item.Name });
        }

        private void InitializeCommands()
        {
            AddWhiteCard = new RelayCommand(() =>
            {
                if (string.IsNullOrWhiteSpace(WhiteCard) || WhiteCards.Any(x => x.Text == WhiteCard.Trim())) return;
                WhiteCards.Add(new WhiteCardViewModel { Text = WhiteCard.Trim() });
                WhiteCard = string.Empty;
                SaveDeck.RaiseCanExecuteChanged();
            });

            AddBlackCard = new RelayCommand(() =>
            {
                var regex = new Regex("_+");
                if (string.IsNullOrWhiteSpace(BlackCard)) return;
                string normalizedText = regex.Replace(BlackCard, "____").Trim();
                if (BlackCards.Any(x => x.Text == normalizedText)) return;
                BlackCards.Add(new BlackCardViewModel { Text = normalizedText, Pick = Pick });
                BlackCard = string.Empty;
                Pick = 1;
                SaveDeck.RaiseCanExecuteChanged();
            });

            RemoveWhiteCard = new RelayCommand(() =>
            {
                WhiteCards.Remove(SelectedWhiteCard);
                SelectedWhiteCard = null;
                SaveDeck.RaiseCanExecuteChanged();
            });

            RemoveBlackCard = new RelayCommand(() =>
            {
                BlackCards.Remove(SelectedBlackCard);
                SelectedBlackCard = null;
                SaveDeck.RaiseCanExecuteChanged();
            });

            SaveDeck = new RelayCommand(async () =>
            {
                if (!string.IsNullOrWhiteSpace(DeckName))
                {
                    if(db.Decks.Any(x => x.Name == DeckName))
                    {
                        var md = new MessageDialog($"This will overwrite deck {DeckName}.", "Warning")
                            { DefaultCommandIndex = 0, CancelCommandIndex = 1 };
                        md.Commands.Add(new UICommand("Continue"));
                        md.Commands.Add(new UICommand("Cancel"));
                        var result = await md.ShowAsync();

                        if (result.Label == "Cancel") return;
                        else
                        {
                            db.Decks.Remove(db.Decks.Single(e => e.Name == DeckName));
                            await db.SaveChangesAsync();
                        }
                    }

                    var deck = new Deck { Name = DeckName };
                    deck.BlackCards = BlackCards.Select(x => new BlackCard { Text = x.Text, Pick = (byte)x.Pick, Deck = deck }).ToList();
                    deck.WhiteCards = WhiteCards.Select(x => new WhiteCard { Text = x.Text, Deck = deck }).ToList();
                    db.Decks.Add(deck);
                    await db.SaveChangesAsync();
                    MessengerInstance.Send(new DeckAddedMessage(deck));
                    LoadDecks();
                    ClearDeck.Execute(null);
                }
            }, CanSaveDeck);

            ClearDeck = new RelayCommand(() =>
            {
                DeckName = string.Empty;
                WhiteCard = string.Empty;
                BlackCard = string.Empty;
                Pick = 1;
                WhiteCards.Clear();
                BlackCards.Clear();
                SelectedBlackCard = null;
                SelectedWhiteCard = null;
                SaveDeck.RaiseCanExecuteChanged();
            });

            LoadDeck = new RelayCommand(async () =>
            {
                var deckFromDb = await db.Decks.Include(e => e.WhiteCards)
                    .Include(e => e.BlackCards).SingleOrDefaultAsync(e => e.Id == DeckToLoad.Id);
                if(deckFromDb != null)
                {
                    ClearDeck.Execute(null);
                    DeckName = deckFromDb.Name;
                    foreach (var item in deckFromDb.WhiteCards.Select(e => new WhiteCardViewModel { Text = e.Text }))
                        WhiteCards.Add(item);
                    foreach (var item in deckFromDb.BlackCards.Select(e => new BlackCardViewModel { Text = e.Text, Pick = e.Pick }))
                        BlackCards.Add(item);
                    SaveDeck.RaiseCanExecuteChanged();
                }
                else
                {
                    var md = new MessageDialog("There is no such deck.", "Error");
                    await md.ShowAsync();
                }
            }, () => DeckToLoad != null);
        }
    }

    class DeckComboBoxItemViewModel
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public override string ToString() => Name;
    }
}
