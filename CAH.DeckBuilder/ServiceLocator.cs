﻿using CAH.DeckBuilder.ViewModels;
using GalaSoft.MvvmLight.Ioc;

namespace CAH.DeckBuilder
{
    class ServiceLocator
    {
        public ServiceLocator()
        {
            Initialize();
        }

        private void Initialize()
        {
            var db = new DeckBuilderDbContext();
            db.Database.EnsureCreated();
            SimpleIoc.Default.Register(() => db);

            SimpleIoc.Default.Register<DeckBuilderViewModel>();
            SimpleIoc.Default.Register<DeckBrowserViewModel>();
        }

        public DeckBuilderViewModel DeckBuilderViewModel => SimpleIoc.Default.GetInstance<DeckBuilderViewModel>();

        public DeckBrowserViewModel DeckBrowserViewModel => SimpleIoc.Default.GetInstance<DeckBrowserViewModel>();
    }
}
