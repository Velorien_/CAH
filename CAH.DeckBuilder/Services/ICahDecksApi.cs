﻿using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CAH.DeckBuilder.Services
{
    interface ICahDecksApi
    {
        [Get("/api/decks/full")]
        Task<IEnumerable<Deck>> GetDecks();

        [Post("/api/decks/import")]
        Task PostDeck(object deck, [Header("x-import-token")] string token);
    }
}
