﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CAH.DeckBuilder
{
    class DeckBuilderDbContext : DbContext 
    {
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlite("Data Source=cahdb.db");
        }

        public DbSet<Deck> Decks { get; set; }
        public DbSet<WhiteCard> WhiteCards { get; set; }
        public DbSet<BlackCard> BlackCards { get; set; }
    }

    class EntityBase
    {
        public int Id { get; set; }
    }

    class Deck : EntityBase
    {
        public Deck()
        {
            WhiteCards = new HashSet<WhiteCard>();
            BlackCards = new HashSet<BlackCard>();
        }

        public string Name { get; set; }

        public IEnumerable<WhiteCard> WhiteCards { get; set; }

        public IEnumerable<BlackCard> BlackCards { get; set; }
    }

    class Card : EntityBase
    {
        public string Text { get; set; }

        [JsonIgnore]
        public virtual Deck Deck { get; set; }
        public int DeckId { get; set; }
    }

    class WhiteCard : Card { }

    class BlackCard : Card
    {
        public byte Pick { get; set; }
    }
}
