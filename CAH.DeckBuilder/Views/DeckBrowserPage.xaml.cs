﻿using CAH.DeckBuilder.ViewModels;
using Windows.UI.Xaml.Controls;

namespace CAH.DeckBuilder.Views
{
    public sealed partial class DeckBrowserPage : Page
    {
        public DeckBrowserPage()
        {
            this.InitializeComponent();
        }

        private void CahUrl_TextChanged(object sender, TextChangedEventArgs e)
        {
            (DataContext as DeckBrowserViewModel).CahUrl = (sender as TextBox).Text;
        }
    }
}
