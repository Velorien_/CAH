﻿using CAH.DeckBuilder.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace CAH.DeckBuilder.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DeckBuilderPage : Page
    {
        public DeckBuilderPage()
        {
            this.InitializeComponent();
        }

        private void GridView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var item in e.AddedItems) (item as CardViewModel).EditMode = true;
            foreach (var item in e.RemovedItems) (item as CardViewModel).EditMode = false;
        }

        private void AddWhiteCard_Click(object sender, RoutedEventArgs e)
        {
            ParentPivot.SelectedIndex = 0;
            (DataContext as DeckBuilderViewModel).AddWhiteCard.Execute(null);
        }

        private void AddBlackCard_Click(object sender, RoutedEventArgs e)
        {
            ParentPivot.SelectedIndex = 1;
            (DataContext as DeckBuilderViewModel).AddBlackCard.Execute(null);
        }
    }
}
