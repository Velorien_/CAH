﻿using CAH.GameLogic.Models;
using CAH.GameLogic.TransferObjects;
using System;
using System.Collections.Generic;

namespace CAH.GameLogic
{
    public class Notifier
    {
        public Action<Player, PlayerUpdateObject> PlayerJoined { get; set; }

        public Action<Player, Player> PlayerLeft { get; set; }

        public Action<Player, IEnumerable<WhiteCard>> HandUpdated { get; set; }

        public Action<Player, string> CardsSelected { get; set; }

        public Action<Player, IEnumerable<PlayerCardsRevealed>> CardsRevealed { get; set; }

        public Action<Player, RoundVictorySummary> RoundWinnerSelected { get; set; }

        public Action<Player, BlackCard> BlackCardDrawn { get; set; }

        public Action<Player, Message> MessageSent { get; set; }

        public Action<Player, string> CzarSelected { get; set; }

        public Action<Player> GameHalted { get; set; }

        public Action<string> GameEnded { get; set; }

        public Action<Player, Player> GameWon { get; set; }

        public Action<Player, string> Error { get; set; }

        public Action<Player, BroadcastableRuleset> Ruleset { get; set; }

        public Action<Player, PlayerScoreUpdate> PlayerScoreUpdated { get; set; }
    }
}
