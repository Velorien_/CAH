﻿using System;
using System.Collections.Generic;
using CAH.GameLogic.Models;
using System.Linq;
using CAH.GameLogic.Extensions;
using CAH.GameLogic.TransferObjects;
using System.Threading.Tasks;

namespace CAH.GameLogic
{
    public class GameState
    {
        private readonly List<Player> _players = new List<Player>();
        private readonly List<Player> _playerQueue = new List<Player>();
        private readonly Ruleset _ruleset;
        private readonly Notifier _notifier;
        private readonly List<BlackCard> _blackCards = new List<BlackCard>();
        private readonly List<WhiteCard> _whiteCards = new List<WhiteCard>();
        private readonly List<WhiteCard> _whiteDiscardPile = new List<WhiteCard>();
        private readonly List<BlackCard> _blackDiscardPile = new List<BlackCard>();
        private Player _czar;
        private BlackCard _currentBlackCard;
        private object _stateLock = new object();

        public GameState(string name, string id, bool isHidden, IEnumerable<BlackCard> blackCards, IEnumerable<WhiteCard> whiteCards, Ruleset ruleset, Notifier notifier)
        {
            Name = name;
            IsHidden = isHidden;
            Id = id;
            _blackCards.AddRange(blackCards);
            _blackCards.Shuffle();
            _whiteCards.AddRange(whiteCards);
            _whiteCards.Shuffle();
            _ruleset = ruleset;
            _notifier = notifier;

            Task.Run(async () =>
            {
                await Task.Delay(TimeSpan.FromMinutes(5));
                if (!(_playerQueue.Any() || _players.Any()))
                {
                    _notifier.GameEnded(Id);
                }
            });
        }

        public string Id { get; }

        public string Name { get; }

        public bool IsHidden { get; }

        public DateTime CreatedAt { get; } = DateTime.Now;

        public GameInfo GameInfo => new GameInfo(Name, _players.Count, _playerQueue.Count, Id);

        public bool HasPlayerWithId(string playerId)
        {
            return _players.Any(x => x.Id == playerId) || _playerQueue.Any(x => x.Id == playerId);
        }

        public bool SelectCards(string playerId, IEnumerable<int> cardIds)
        {
            lock (_stateLock)
            {
                if (cardIds.Count() != _currentBlackCard.Pick)
                {
                    throw new InvalidOperationException($"Wrong number of cards picked. Was {cardIds.Count()}, should be {_currentBlackCard.Pick}");
                }

                var player = GetActivePlayerById(playerId);

                var cards = cardIds.Select(x => player.Hand.First(c => c.Id == x)).ToList();

                if (cards.Count() != cardIds.Count())
                {
                    throw new InvalidOperationException("Player cards mismatch");
                }

                player.SelectedCards.AddRange(cards);
                foreach (var card in cards)
                {
                    player.Hand.Remove(card);
                }

                NotifyAllPlayers(_notifier.CardsSelected, playerId);
                NotifyAllPlayers(_notifier.MessageSent, new Message("info", $"{player.Name} selected their card{(cardIds.Count() > 1 ? "s" : string.Empty)}", "SYSTEM"));

                return ShouldRevealCards();
            }
        }

        public IEnumerable<string> GetPlayerIds()
        {
            lock (_stateLock)
            {
                return _players.Select(x => x.Id);
            }
        }

        public (bool shouldStartNewRound, bool shouldRevealCards) RemovePlayer(string playerId)
        {
            lock (_stateLock)
            {
                var player = _playerQueue.FirstOrDefault(x => x.Id == playerId);
                if (player != null)
                {
                    _playerQueue.Remove(player);
                }
                else
                {
                    player = GetActivePlayerById(playerId);
                    _players.Remove(player);
                }

                NotifyAllPlayers(_notifier.PlayerLeft, player);
                DiscardPlayerCards(player);
                int totalPlayers = _players.Count + _playerQueue.Count;
                if (totalPlayers == 0)
                {
                    _notifier.GameEnded(Id);
                    return (false, false);
                }
                else if (totalPlayers < 3)
                {
                    NotifyAllPlayers(_notifier.GameHalted);
                    _playerQueue.AddRange(_players);
                    _players.Clear();
                    _czar = null;
                    return (false, false);
                }
                else if (_players.Count < 3 || player == _czar)
                {
                    _czar = null;
                    return (true, false);
                }
                else if (ShouldRevealCards())
                {
                    return (false, true);
                }
                else
                {
                    return (false, false);
                }
            }
        }

        public bool AddPlayer(string playerId, string name)
        {
            lock (_stateLock)
            {
                if (_playerQueue.Any(x => x.Name == name) || _players.Any(x => x.Name == name))
                {
                    _notifier.Error(new Player(playerId, string.Empty), "This name is already taken");
                    return false;
                }

                _playerQueue.Add(new Player(playerId, name));
                NotifyAllPlayers(_notifier.MessageSent, new Message("info", $"{name} joins the game.", "SYSTEM"));
                // should start the game?
                return !_players.Any() && _playerQueue.Count >= _ruleset.MinStartPlayers;
            }
        }

        public void RevealCards()
        {
            lock (_stateLock)
            {
                var cardsRevealed = _players.Where(x => x != _czar).Select(PlayerCardsRevealed.FromModel).ToList();
                cardsRevealed.Shuffle();
                NotifyAllPlayers(_notifier.CardsRevealed, cardsRevealed);
            }
        }

        public bool SelectWinner(string playerId)
        {
            lock (_stateLock)
            {
                var player = GetActivePlayerById(playerId);
                player.Score++;
                NotifyAllPlayers(_notifier.RoundWinnerSelected, new RoundVictorySummary(player.Name, _currentBlackCard, player.SelectedCards));
                string cards = string.Join(", ", player.SelectedCards.Select(x => $@"""{x.Text}"""));
                NotifyAllPlayers(_notifier.MessageSent, new Message("info", $"{player.Name} wins the round with {cards}", "SYSTEM"));
                if (player.Score == _ruleset.MaxPoints)
                {
                    NotifyAllPlayers(_notifier.GameWon, player);
                    return false;
                }

                return true;
            }
        }

        public void StartNewRound()
        {
            lock (_stateLock)
            {
                _ruleset.MinStartPlayers = 3;
                MovePlayersFromQueue();
                NotifyAllPlayers(_notifier.Ruleset, _ruleset.BroadcastableRules);
                DiscardSelectedCards();
                _players.ForEach(p => DealCards(p, _ruleset.HandSize - p.Hand.Count));
                DrawBlackCard();
                SelectNewCzar();
                NotifyAllPlayers(_notifier.MessageSent, new Message("info", $"New round begins, {_czar.Name} is the new Czar.", "SYSTEM"));
            }
        }

        public void DiscardHand(string playerId)
        {
            if (!_ruleset.AllowDiscarding)
            {
                return;
            }

            lock (_stateLock)
            {
                var player = GetActivePlayerById(playerId);
                if (player != null && !player.SelectedCards.Any() && player.Score > 0)
                {
                    DiscardPlayerCards(player);
                    DealCards(player, _ruleset.HandSize);
                    player.Score--;
                    SendMessage("info", $"{player.Name} has discarded their hand", "SYSTEM");
                    NotifyAllPlayers(_notifier.PlayerScoreUpdated, new PlayerScoreUpdate(player.Id, player.Score));
                }
            }
        }

        public void MessagePlayers(string type, string text, string name)
        {
            lock (_stateLock)
            {
                SendMessage(type, text, name);
            }
        }

        private Player GetActivePlayerById(string playerId)
        {
            var player = _players.FirstOrDefault(x => x.Id == playerId);
            if (player == null)
            {
                throw new InvalidOperationException($"There's no player with such id: {playerId}");
            }

            return player;
        }

        private void SendMessage(string type, string text, string name)
        {
            var message = new Message(type, text, name);
            NotifyAllPlayers(_notifier.MessageSent, message);
        }

        private void RestoreDiscardPile()
        {
            _whiteDiscardPile.Shuffle();
            _whiteCards.AddRange(_whiteDiscardPile);
            _whiteDiscardPile.Clear();
        }

        private void DiscardPlayerCards(Player player)
        {
            _whiteDiscardPile.AddRange(player.SelectedCards);
            _whiteDiscardPile.AddRange(player.Hand);
            player.SelectedCards.Clear();
            player.Hand.Clear();
        }

        private void DealCards(Player player, int numberOfCards)
        {
            if (_whiteCards.Count < numberOfCards)
            {
                RestoreDiscardPile();
            }

            player.Hand.AddRange(_whiteCards.Take(numberOfCards));
            try
            {
                _whiteCards.RemoveRange(0, numberOfCards);
            }
            catch
            {
                NotifyAllPlayers(_notifier.Error, "Not enough white cards for this number of players");
            }

            _notifier.HandUpdated(player, player.Hand);
        }

        private void MovePlayersFromQueue()
        {
            _playerQueue.Shuffle();
            _players.AddRange(_playerQueue);
            _playerQueue.Clear();
            NotifyAllPlayers(_notifier.PlayerJoined, new PlayerUpdateObject(Name, _players.Select(PlayerListItem.FromModel)));
        }

        private void DrawBlackCard()
        {
            if (!_blackCards.Any())
            {
                _blackCards.AddRange(_blackDiscardPile);
                _blackCards.Shuffle();
            }

            if (_currentBlackCard != null)
            {
                _blackDiscardPile.Add(_currentBlackCard);
            }

            _currentBlackCard = _blackCards.First();
            _blackCards.RemoveAt(0);
            NotifyAllPlayers(_notifier.BlackCardDrawn, _currentBlackCard);
        }

        private void SelectNewCzar()
        {
            if (_czar == null)
            {
                _czar = _players.First();
            }
            else
            {
                int czarIndex = _players.IndexOf(_czar);
                _czar = _players.ElementAt((czarIndex + 1) % _players.Count);
            }

            NotifyAllPlayers(_notifier.CzarSelected, _czar.Id);
        }

        private void DiscardSelectedCards()
        {
            foreach (var p in _players)
            {
                _whiteDiscardPile.AddRange(p.SelectedCards);
                p.SelectedCards.Clear();
            }
        }

        private void NotifyAllPlayers(Action<Player> action) => _players.ForEach(action);

        private bool ShouldRevealCards() => _players.Count(x => x.SelectedCards.Any()) == _players.Count - 1;

        private void NotifyAllPlayers<T1, T2>(Action<T1, T2> action, T2 argument) where T1 : Player =>
            _players.ForEach(player => action(player as T1, argument));
    }
}
