﻿using System.Collections.Generic;

namespace CAH.GameLogic.Models
{
    public class Player
    {
        public Player(string id, string name)
        {
            Id = id;
            Name = name;
        }

        public string Id { get; }

        public List<WhiteCard> Hand { get; } = new List<WhiteCard>();

        public List<WhiteCard> SelectedCards { get; } = new List<WhiteCard>();

        public int Score { get; set; }

        public string Name { get; }
    }
}
