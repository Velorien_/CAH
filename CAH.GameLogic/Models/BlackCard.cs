﻿namespace CAH.GameLogic.Models
{
    public class BlackCard
    {
        public BlackCard(int id, byte pick, string text)
        {
            Id = id;
            Pick = pick;
            Text = text;
        }

        public int Id { get; }

        public byte Pick { get; }

        public string Text { get; }
    }
}
