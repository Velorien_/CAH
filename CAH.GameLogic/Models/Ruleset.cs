﻿namespace CAH.GameLogic.Models
{
    public class Ruleset
    {
        private BroadcastableRuleset _broadcastableRules;

        public Ruleset(int handSize, int maxPoints, int minStartPlayers, bool allowDiscarding)
        {
            HandSize = handSize;
            MaxPoints = maxPoints;
            MinStartPlayers = minStartPlayers;
            AllowDiscarding = allowDiscarding;
        }

        public int HandSize { get; }

        public int MaxPoints { get; }

        public int MinStartPlayers { get; set; }

        public bool AllowDiscarding { get; set; }

        public BroadcastableRuleset BroadcastableRules => _broadcastableRules ?? (_broadcastableRules = new BroadcastableRuleset(this));
    }

    public class BroadcastableRuleset
    {
        public BroadcastableRuleset(Ruleset ruleset)
        {
            HandSize = ruleset.HandSize;
            AllowDiscarding = ruleset.AllowDiscarding;
        }

        public int HandSize { get; set; }

        public bool AllowDiscarding { get; set; }
    }
}
