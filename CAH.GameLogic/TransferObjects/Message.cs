﻿using System;

namespace CAH.GameLogic.TransferObjects
{
    public class Message
    {
        public Message(string messageType, string text, string name)
        {
            MessageType = messageType;
            Text = text;
            Name = name;
        }

        public string MessageType { get; }

        public string Text { get; }

        public string Name { get; }
    }
}
