﻿namespace CAH.GameLogic.TransferObjects
{
    public class PlayerScoreUpdate
    {
        public PlayerScoreUpdate(string playerId, int score)
        {
            PlayerId = playerId;
            Score = score;
        }

        public int Score { get; set; }

        public string PlayerId { get; set; }
    }
}
