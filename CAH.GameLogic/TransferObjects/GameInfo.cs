﻿namespace CAH.GameLogic.TransferObjects
{
    public class GameInfo
    {
        public GameInfo(string name, int players, int queue, string id)
        {
            Name = name;
            Players = players;
            Queue = queue;
            Id = id;
        }

        public string Name { get; }

        public int Players { get; }

        public int Queue { get; }

        public string Id { get; }
    }
}
