﻿using CAH.GameLogic.Models;

namespace CAH.GameLogic.TransferObjects
{
    public class PlayerListItem
    {
        private PlayerListItem(string name, string id, int score)
        {
            Name = name;
            Id = id;
            Score = score;
        }

        public string Name { get; }

        public string Id { get; }

        public int Score { get; }

        public bool DoneSelecting { get; }

        public static PlayerListItem FromModel(Player player) =>
            new PlayerListItem(player.Name, player.Id, player.Score);
    }
}
