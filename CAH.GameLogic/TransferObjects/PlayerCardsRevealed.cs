﻿using CAH.GameLogic.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CAH.GameLogic.TransferObjects
{
    public class PlayerCardsRevealed
    {
        private PlayerCardsRevealed(string playerId, IEnumerable<WhiteCard> selectedCards)
        {
            PlayerId = playerId;
            SelectedCards = selectedCards;
        }

        public string PlayerId { get; }

        public bool Selected { get; }

        public IEnumerable<WhiteCard> SelectedCards { get; }

        public static PlayerCardsRevealed FromModel(Player player) => new PlayerCardsRevealed(player.Id, player.SelectedCards);
    }
}
