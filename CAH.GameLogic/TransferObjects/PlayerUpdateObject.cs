﻿using System.Collections.Generic;

namespace CAH.GameLogic.TransferObjects
{
    public class PlayerUpdateObject
    {
        public PlayerUpdateObject(string gameName, IEnumerable<PlayerListItem> players)
        {
            GameName = gameName;
            Players = players;
        }

        public string GameName { get; }

        public IEnumerable<PlayerListItem> Players { get; }
    }
}
