﻿using CAH.GameLogic.Models;
using System.Collections.Generic;

namespace CAH.GameLogic.TransferObjects
{
    public class RoundVictorySummary
    {
        public RoundVictorySummary(string name, BlackCard blackCard, IEnumerable<WhiteCard> whiteCards)
        {
            Name = name;
            BlackCard = blackCard;
            WhiteCards = whiteCards;
        }

        public string Name { get; }

        public BlackCard BlackCard { get; }

        public IEnumerable<WhiteCard> WhiteCards { get; }
    }
}
