﻿using CAH.Web.Entities;
using CAH.Web.Services;
using CAH.Web.TransferObjects;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CAH.Web.Api
{
    public class DecksController : Controller
    {
        private readonly DeckService _deckService;

        public DecksController(DeckService deckService)
        {
            _deckService = deckService;
        }

        [HttpGet("api/decks")]
        public IEnumerable<DeckInfo> GetInfo() => _deckService.GetDecksInfo();

        [HttpGet("api/decks/full")]
        public IEnumerable<DeckEntity> GetFullInfo() => _deckService.GetDecks();

        [HttpGet("api/decks/{id}")]
        public DeckEntity GetDeck(int id) => _deckService.GetDecksById(new[] { id }).FirstOrDefault();

        [HttpGet("api/decks/import")]
        public IActionResult Import()
        {
            string token = Request.Headers["x-import-token"].FirstOrDefault();
            if (token != Environment.GetEnvironmentVariable("IMPORT_TOKEN"))
            {
                return Forbid();
            }

            _deckService.ImportCards();
            return Ok();
        }

        [HttpPost("/api/decks/import")]
        public IActionResult PostImport([FromBody] DeckImportObject deck)
        {
            string token = Request.Headers["x-import-token"].FirstOrDefault();
            if (token != Environment.GetEnvironmentVariable("IMPORT_TOKEN"))
            {
                return Forbid();
            }

            if (deck == null)
            {
                return BadRequest("Input data is in invalid format");
            }

            if (_deckService.ImportDeck(deck))
            {
                return Ok();
            }
            else
            {
                return BadRequest("There's already a deck with such name");
            }
        }

        [HttpDelete("api/decks/{id}")]
        public IActionResult Delete(int id)
        {
            string token = Request.Headers["x-import-token"].FirstOrDefault();
            if (token != Environment.GetEnvironmentVariable("IMPORT_TOKEN"))
            {
                return Forbid();
            }

            _deckService.DeleteDeck(id);
            return Ok();
        }
    }
}
