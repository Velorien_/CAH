﻿using CAH.GameLogic.TransferObjects;
using CAH.Web.Services;
using CAH.Web.TransferObjects;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CAH.Web.Api
{
    [Route("api/games")]
    public class GamesController : Controller
    {
        private readonly GameService _gameService;
        private readonly DeckService _cardService;

        public GamesController(GameService gameService, DeckService cardService)
        {
            _gameService = gameService;
            _cardService = cardService;
        }

        [HttpGet]
        public IEnumerable<GameInfo> Get() => _gameService.GetGameInfo();

        [HttpPost]
        public IActionResult Post([FromBody] GameDescriptor gameDescriptor)
        {
            if(gameDescriptor == null)
            {
                return BadRequest(new { errors = new[] { "Something is fishy..." } });
            }

            var errors = ValidateGame(gameDescriptor);
            if(errors.Any())
            {
                return BadRequest(new { errors });
            }

            var decks = _cardService.GetDecksById(gameDescriptor.DeckIds);
            if(decks.Count() != gameDescriptor.DeckIds.Count())
            {
                return BadRequest(new { errors = new[] { "There are no such decks" } });
            }

            var blackCards = decks.SelectMany(x => x.BlackCards).Select(x => x.ToModel());
            var whiteCards = decks.SelectMany(x => x.WhiteCards).Select(x => x.ToModel());
            return Ok(new { gameId = _gameService.CreateGame(gameDescriptor, whiteCards, blackCards) });
        }

        private IEnumerable<string> ValidateGame(GameDescriptor gameDescriptor)
        {
            var errors = new List<string>();
            gameDescriptor.GameName = gameDescriptor.GameName?.Trim();
            if(string.IsNullOrWhiteSpace(gameDescriptor.GameName))
            {
                errors.Add("Game name cannot be empty");
            }

            if(gameDescriptor.GameName.Length > 20)
            {
                errors.Add("Name is too long. Max 20 characters are allowed");
            }

            if (!gameDescriptor.DeckIds.Any())
            {
                errors.Add("You must choose some decks to play");
            }

            if(gameDescriptor.HandSize < 1)
            {
                errors.Add("Players need at least one card in hand");
            }

            if(gameDescriptor.HandSize > 15)
            {
                errors.Add($"{gameDescriptor.HandSize} cards? How about no?");
            }

            if(gameDescriptor.MaxPoints < 0)
            {
                errors.Add("The game would be awfully quick with such score for victory");
            }

            if (gameDescriptor.MinPlayers < 3)
            {
                errors.Add("You need to have at least 3 players to play this game");
            }

            return errors;
        }
    }
}
