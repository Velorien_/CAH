﻿using CAH.GameLogic.Models;
using Newtonsoft.Json;

namespace CAH.Web.Entities
{
    public class BlackCardEntity
    {
        public int Id { get; set; }

        public byte Pick { get; set; }

        public string Text { get; set; }

        [JsonIgnore]
        public DeckEntity Deck { get; set; }

        public BlackCard ToModel() => new BlackCard(Id, Pick, Text);
    }
}
