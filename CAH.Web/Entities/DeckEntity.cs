﻿using System.Collections.Generic;

namespace CAH.Web.Entities
{
    public class DeckEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<WhiteCardEntity> WhiteCards { get; set; }

        public IEnumerable<BlackCardEntity> BlackCards { get; set; }
    }
}
