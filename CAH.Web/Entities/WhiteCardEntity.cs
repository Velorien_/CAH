﻿using CAH.GameLogic.Models;
using Newtonsoft.Json;

namespace CAH.Web.Entities
{
    public class WhiteCardEntity
    {
        public int Id { get; set; }

        public string Text { get; set; }

        [JsonIgnore]
        public DeckEntity Deck { get; set; }

        public WhiteCard ToModel() => new WhiteCard(Id, Text);
    }
}
