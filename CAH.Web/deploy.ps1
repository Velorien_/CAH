dotnet restore
dotnet publish -c Release -o out
docker build -t cah .
docker tag cah registry.heroku.com/cardsofspite/web
docker push registry.heroku.com/cardsofspite/web
Remove-Item -Recurse out