﻿using CAH.Web.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CAH.Web.TransferObjects;

namespace CAH.Web.Services
{
    public class DeckService
    {
        private readonly CahDbContext _db;
        private readonly IFileProvider _fileProvider;

        public DeckService(CahDbContext db, IFileProvider fileProvider)
        {
            _db = db;
            _fileProvider = fileProvider;
        }

        public IEnumerable<DeckInfo> GetDecksInfo() => _db.Decks.Select(x => new DeckInfo(x.Id, x.Name, x.WhiteCards.Count(), x.BlackCards.Count()));

        public IEnumerable<DeckEntity> GetDecks() => _db.Decks.Include(x => x.BlackCards).Include(x => x.WhiteCards);

        public IEnumerable<DeckEntity> GetDecksById(IEnumerable<int> ids) =>
            _db.Decks.Include(x => x.WhiteCards).Include(x => x.BlackCards).Where(x => ids.Contains(x.Id)).ToList();

        public void DeleteDeck(int id)
        {
            var deck = _db.Decks.Include(x => x.WhiteCards).Include(x => x.BlackCards).FirstOrDefault(x => x.Id == id);
            if (deck == null)
            {
                return;
            }

            _db.Remove(deck);
            _db.SaveChanges();
        }

        public void ImportCards()
        {
            using (var stream = _fileProvider.GetFileInfo("cards.json").CreateReadStream())
            using (var reader = new StreamReader(stream))
            {
                var json = JObject.Parse(reader.ReadToEnd());
                var properties = json.Properties().Select(x => x.Name).ToList();
                properties.RemoveRange(0, 2);
                _db.Decks.RemoveRange(_db.Decks.Include(x => x.WhiteCards).Include(x => x.BlackCards));
                _db.SaveChanges();

                foreach (var p in properties)
                {
                    var deck = json[p];
                    var white = json["whiteCards"].Skip(deck["white"].FirstOrDefault()?.Value<int>() ?? 0).Take(deck["white"].Count());
                    var black = json["blackCards"].Skip(deck["black"].FirstOrDefault()?.Value<int>() ?? 0).Take(deck["black"].Count());
                    var deckEntity = new DeckEntity
                    {
                        Name = deck["name"].Value<string>()
                    };

                    deckEntity.WhiteCards = white.Select(x => new WhiteCardEntity { Text = x.Value<string>() }).ToList();
                    deckEntity.BlackCards = black.Select(x => new BlackCardEntity { Pick = x.Value<byte>("pick"), Text = x.Value<string>("text") }).ToList();

                    _db.Decks.Add(deckEntity);
                    _db.SaveChanges();
                }

            }
        }

        public bool ImportDeck(DeckImportObject deck)
        {
            if (_db.Decks.Any(x=> x.Name == deck.Name))
            {
                return false;
            }

            var deckEntity = new DeckEntity() { Name = deck.Name };
            deckEntity.WhiteCards = deck.WhiteCards.Select(x => new WhiteCardEntity { Text = x }).ToList();
            deckEntity.BlackCards = deck.BlackCards.Select(x => new BlackCardEntity { Pick = x.Pick, Text = x.Text }).ToList();

            _db.Decks.Add(deckEntity);
            _db.SaveChanges();

            return true;
        }
    }
}
