﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CAH.Web.Services
{
    public class SocketHolder : IDisposable
    {
        private static readonly ArraySegment<byte> PingMessage = CreateMessage(@"{""type"":""Ping""}");
        private readonly Timer timer;
        private int noPongCount = 0;
        private int maxNoPongCount;
        private CancellationTokenSource cancellationTokenSource;
        private readonly WebSocket socket;
        private Action<string> noPongExceededCallback;

        public SocketHolder(WebSocket socket, string id, Action<string> noPongExceededCallback)
        {
            this.socket = socket;
            this.noPongExceededCallback = noPongExceededCallback;
            cancellationTokenSource = new CancellationTokenSource();
            string noPong = Environment.GetEnvironmentVariable("MAX_NO_PONG_COUNT");
            if (noPong == null || !int.TryParse(noPong, out maxNoPongCount))
            {
                maxNoPongCount = 5;
            }

            timer = new Timer(async e =>
            {
                try
                {
                    await socket.SendAsync(PingMessage, WebSocketMessageType.Text, true, cancellationTokenSource.Token);
                    noPongCount++;

                    if (noPongCount > 10)
                    {
                        noPongExceededCallback.Invoke(id);
                        timer.Dispose();
                    }
                }
                catch (WebSocketException) { noPongExceededCallback.Invoke(id); }
                catch (OperationCanceledException) { }
            }, null, 5000, 5000);
        }

        public void ResetNoPongCounter() => noPongCount = 0;

        public async Task SendAsync(string data) => await socket.SendAsync(CreateMessage(data), WebSocketMessageType.Text, true, cancellationTokenSource.Token);

        public async void Dispose()
        {
            cancellationTokenSource.Cancel();
            try { await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", CancellationToken.None); }
            catch (WebSocketException) { }
            socket.Dispose();
        }

        private static ArraySegment<byte> CreateMessage(string data) => new ArraySegment<byte>(Encoding.UTF8.GetBytes(data));
    }
}
