﻿createStates = () => ({
    error: 'error',
    joining: 'joining',
    waiting: 'waiting',
    active: 'active',
    finished: 'finished'
});

var g = new Vue({
    el: '#cah',
    data: {
        socket: null,
        playerId: null,
        gameId: gameId,
        roomName: '',
        name: null,
        nameInvalid: false,
        players: [],
        hand: [],
        selectedCards: [],
        revealedCards: [],
        selectedCount: 0,
        canSelect: true,
        czar: null,
        blackCard: {},
        chat: [],
        chatMessage: null,
        ruleset: {},
        error: null,
        winner: null,
        previousWinner: null,
        previousBlackCard: {},
        previousWhiteCards: [],
        state: 'joining',
        states: createStates()
    },
    computed: {
        canDiscardHand: function () {
            return this.ruleset.allowDiscarding && this.players.some(x => x.id === this.playerId && x.score > 0)
                && this.hand.length === this.ruleset.handSize && this.playerId !== this.czar && this.canSelect
                && !this.selectedCards.length && !this.revealedCards.length;
        }
    },
    methods: {
        joinGame: function () {
            var t = this;
            t.error = null;
            t.nameInvalid = false;
            window.onbeforeunload = () => "Are you sure you want to desert the field of battle?";

            if (!t.name || !t.name.trim()) {
                t.nameInvalid = true;
                return;
            }

            if (!t.socket) {
                var socketHost = '';
                if (window.location.protocol === "https:") {
                    socketHost = 'wss://'+ location.host +'/ws';
                } else {
                    socketHost = 'ws://' + location.host + '/ws';
                }

                t.socket = new WebSocket(socketHost);
                t.socket.onopen = e => {
                    t.state = t.states.waiting;
                    t.socket.send(t.createMessage('join', t.name));
                };
                t.socket.onerror = e => {
                    t.onError({ data: 'Network error occured - connection to game server has been lost.' }, t);
                };
                t.socket.onclose = e => {
                    t.onError({data: 'Connection to game server has been closed.' }, t);
                };
                t.socket.onmessage = e => t.handleMessage(e);
            }
            else {
                t.state = t.states.waiting;
                t.socket.send(t.createMessage('join', t.name));
            }
        },
        createMessage: function (type, data) {
            return JSON.stringify({ type: type, gameId: this.gameId, data: data });
        },
        select: function (card) {
            if (this.selectedCards.length < this.blackCard.pick) {
                this.selectedCards.push(card);
            }
        },
        deselect: function (card) {
            var index = this.selectedCards.indexOf(card);
            if (index !== -1) this.selectedCards.splice(index, 1);
        },
        confirmSelection() {
            if (this.selectedCards.length === this.blackCard.pick) {
                this.socket.send(this.createMessage('cardsSelected', this.selectedCards.map(x => x.id)));
                this.canSelect = false;
                this.selectedCards = [];
            }
        },
        selectWinner: function (group) {
            if (this.playerId === this.czar) {
                for (g of this.revealedCards) {
                    g.selected = group.playerId === g.playerId;
                }
            }
        },
        confirmWinner: function () {
            if (this.playerId === this.czar) {
                for (g of this.revealedCards) {
                    if (g.selected) {
                        this.socket.send(this.createMessage('winnerSelected', g.playerId));
                        return;
                    }
                }
            }
        },
        discardHand: function () {
            this.socket.send(this.createMessage('discardHand'));
        },
        // message handlers
        onError: function (m, t) {
            window.onbeforeunload = () => { };
            if (t.state === t.states.waiting) {
                t.error = m.data;
                t.state = t.states.joining;
            } else if (t.state === t.states.finished) {
                return;
            } else {
                t.state = t.states.error;
                t.error = m.data;
                t.socket.close();
            }
        },
        onPlayerJoined: function (m, t) {
            t.state = t.states.active;
            t.players = m.data.players;
            t.roomName = m.data.gameName;
        },
        onPlayerLeft: function (m, t) {
            var i = 0;
            for (p of t.players) {
                if (p.id === m.data.id) {
                    if (m.data.hand.length > 0) {
                        t.players.splice(i, 1);
                        if (t.selectedCount > 0) {
                            t.selectedCount--;
                        } else {
                            i = 0;
                            for (c of t.selectedCards) {
                                if (c.playerId === m.data.id) {
                                    t.selectedCards.splice(i, 1);
                                }
                                i++;
                            }
                        }
                    }

                    return;
                }

                i++;
            }
        },
        onHandUpdated: function (m, t) {
            t.hand = m.data;
            t.canSelect = true;
        },
        onCardsRevealed: function (m, t) {
            t.selectedCount = 0;
            t.revealedCards = m.data;
            for (player of t.players) {
                player.doneSelecting = false;
            }
        },
        onCardsSelected: function (m, t) {
            t.selectedCount++;
            for (player of t.players) {
                if (player.id === m.data) {
                    player.doneSelecting = true;
                    return;
                }
            }
        },
        onRoundWinnerSelected: function (m, t) {
            t.revealedCards = [];
            t.previousBlackCard = m.data.blackCard;
            t.previousWhiteCards = m.data.whiteCards;
            t.previousWinner = m.data.name;
        },
        onGameHalted: function (m, t) {
            t.state = t.states.waiting;
            t.blackCard = {};
            t.players = [];
            t.selectedCount = 0;
            t.hand = [];
            t.revealedCards = [];
            t.canSelect = true;
            t.czar = null;
        },
        onGameWon: function (m, t) {
            t.state = t.states.finished;
            t.winner = m.data.name;
            t.socket.close();
            window.onbeforeunload = () => { };
        },
        onMessageSent: function (m, t) {
            if (t.chat.length > 100) {
                t.chat.splice(0, 1);
            }

            var date = new Date();
            var minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
            var seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
            m.data.time = `${date.getHours()}:${minutes}:${seconds}`;
            t.chat.push(m.data);
            setTimeout(() => {
                var chatElement = document.getElementById('message-list');
                if (chatElement) chatElement.scrollTop = chatElement.scrollHeight;
            }, 50);
        },
        onHandDiscarded: function (m, t) {
            for (p of this.players) {
                if (p.id === m.data) {
                    p.score--;
                    return;
                }
            }
        },
        onPlayerScoreUpdated: function (m, t) {
            for (p of this.players) {
                if (p.id === m.data.playerId) {
                    p.score--;
                    return;
                }
            }
        },
        handleMessage: function (message) {
            var t = this;
            if (t.state === t.states.finished) return;
            var m = JSON.parse(message.data);
            switch (m.type) {
                case 'Error':
                    t.onError(m, t);
                    break;
                case 'PlayerJoined':
                    t.onPlayerJoined(m, t);
                    break;
                case 'PlayerLeft':
                    t.onPlayerLeft(m, t);
                    break;
                case 'HandUpdated':
                    t.onHandUpdated(m, t);
                    break;
                case 'CardsSelected':
                    t.onCardsSelected(m, t);
                    break;
                case 'CardsRevealed':
                    t.onCardsRevealed(m, t)
                    break;
                case 'RoundWinnerSelected':
                    t.onRoundWinnerSelected(m, t);
                    break;
                case 'BlackCardDrawn':
                    t.blackCard = m.data;
                    break;
                case 'CzarSelected':
                    t.czar = m.data;
                    break;
                case 'GameHalted':
                    t.onGameHalted(m, t);
                    break;
                case 'GameWon':
                    t.onGameWon(m, t);
                    break;
                case 'MessageSent':
                    t.onMessageSent(m, t);
                    break;
                case 'IdAssigned':
                    this.playerId = m.data;
                    break;
                case 'Ruleset':
                    t.ruleset = m.data;
                    break;
                case 'HandDiscarded':
                    t.onHandDiscarded(m, t);
                    break;
                case 'PlayerScoreUpdated':
                    t.onPlayerScoreUpdated(m, t);
                    break;
                case 'Ping':
                    t.socket.send(t.createMessage('pong', null));
                    break;
            }
        },
        sendMessage: function () {
            if (this.chatMessage) {
                this.socket.send(this.createMessage('chat', { text: this.chatMessage, name: this.name }));
                this.chatMessage = null;
            }
        }
    }
});