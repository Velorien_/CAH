﻿var vm = new Vue({
    el: '#cah',
    data: {
        decks: [],
        currentDeck: {},
        showBlack: true
    },
    methods: {
        getDecks: function () {
            var t = this;
            fetch('/api/decks')
                .then(response => { if (response.ok) return response.json(); console.log(response); })
                .then(data => {
                    t.decks = data;
                });
        },
        selectDeck: function (id) {
            var t = this;
            fetch('/api/decks/' + id)
                .then(response => { if (response.ok) return response.json(); console.log(response); })
                .then(data => { t.currentDeck = data; });
        }
    },
    mounted: function () {
        this.getDecks();
    }
});