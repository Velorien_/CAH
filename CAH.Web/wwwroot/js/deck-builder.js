﻿var vm = new Vue({
    el: '#cah',
    data: {
        deck: {
            name: '',
            whiteCards: [],
            blackCards: []
        },
        currentWhiteCard: '',
        currentBlackCard: { pick: 1, text: '' },
        tabs: { white: 'white', black: 'black', json: 'json', loadJson: 'loadJson' },
        tab: 'white',
        whiteCardError: false,
        blackCardError: false,
        inputError: false,
        inputJson: ''
    },
    computed: {
        json: function() {
            return JSON.stringify(this.deck, null, '    ');
        }
    },
    methods: {
        addWhiteCard: function () {
            this.tab = this.tabs.white;
            this.whiteCardError = false;
            this.currentWhiteCard = this.currentWhiteCard.trim();
            if (this.currentWhiteCard != '' && this.deck.whiteCards.indexOf(this.currentWhiteCard) < 0) {
                this.deck.whiteCards.push(this.currentWhiteCard);
                this.currentWhiteCard = '';
            } else {
                this.whiteCardError = true;
            }
        },
        addBlackCard: function () {
            this.tab = this.tabs.black;
            this.blackCardError = false;
            this.currentBlackCard.text = this.currentBlackCard.text.trim();
            if (this.currentBlackCard.text == '' ||
                this.currentBlackCard.pick != parseInt(this.currentBlackCard.pick)) {
                this.blackCardError = true; return;
            }

            for (c of this.deck.blackCards) {
                if (c.text == this.currentBlackCard.text) {
                    this.blackCardError = true; return;
                }
            }

            this.deck.blackCards.push(this.currentBlackCard);
            this.currentBlackCard = { pick: 1, text: '' };
        },
        removeWhiteCard: function (index) {
            this.whiteCards.splice(index, 1);
        },
        removeBlackCard: function (index) {
            this.blackCards.splice(index, 1);
        },
        addNewlines: function (text) {
            return text.replace(/\n/g, "<br/>");
        },
        loadJson: function () {
            this.inputError = false;
            try {
                var obj = JSON.parse(this.inputJson);
                if (obj.name && obj.whiteCards && obj.blackCards) {
                    this.deck = obj;
                } else this.inputError = true;
            } catch (err) {
                this.inputError = true;
            }
        }
    }
});