﻿var ws = new Vue({
    el: '#cah',
    data: {
        games: [],
        decks: [],
        errors: null,
        gettingGames: false,
        gettingDecks: false,
        creatingGame: false,
        gameName: null,
        handSize: 10,
        maxPoints: 15,
        minPlayers: 3,
        selectedWhite: 0,
        selectedBlack: 0,
        allowDiscarding: false,
        maxPointsInvalid: false,
        minPlayersInvalid: false,
        handSizeInvalid: false,
        gameNameInvalid: false,
        isHidden: false,
        socket: null
    },
    methods: {
        getGames: function () {
            var t = this;
            t.gettingGames = true;
            t.games = [];
            fetch('/api/games')
                .then(response => { if (response.ok) return response.json(); console.log(response); t.gettingGames = false; })
                .then(data => {
                    t.games = data;
                    t.gettingGames = false;
                });
        },
        getDecks: function () {
            var t = this;
            t.gettingDecks = true;
            fetch('/api/decks')
                .then(response => { if (response.ok) return response.json(); console.log(response); t.gettingDecks = false; })
                .then(data => {
                    for (deck of data) {
                        deck.selected = false;
                    }
                    t.decks = data;
                    t.gettingDecks = false;
                });
        },
        createGame: function () {
            var t = this;
            if (t.creatingGame) return;

            t.errors = null;
            t.handSizeInvalid = false;
            t.maxPointsInvalid = false;
            t.gameNameInvalid = false;
            t.minPlayersInvalid = false;
            var gameDescriptor = {
                GameName: t.gameName,
                DeckIds: t.getSelectedDeckIds(),
                IsHidden: t.isHidden,
                MaxPoints: parseInt(t.maxPoints),
                HandSize: parseInt(t.handSize),
                MinPlayers: parseInt(t.minPlayers),
                AllowDiscarding: t.allowDiscarding
            };

            t.gameNameInvalid = !gameDescriptor.GameName || !gameDescriptor.GameName.trim();
            t.handSizeInvalid = !gameDescriptor.HandSize;
            t.maxPointsInvalid = !gameDescriptor.MaxPoints;
            t.minPlayersInvalid = !gameDescriptor.MinPlayers;

            if (t.gameNameInvalid || t.handSizeInvalid || t.maxPointsInvalid || t.minPlayersInvalid) {
                return;
            }

            t.creatingGame = true;
            fetch('/api/games', {
                headers: { 'Content-Type': 'application/json' },
                method: 'POST',
                body: JSON.stringify(gameDescriptor)
            }).then(response => response.json())
                .then(data => {
                    if (data.errors) {
                        t.errors = data.errors;
                    } else {
                        window.location.href = '/Game/' + data.gameId;
                    }

                    t.creatingGame = false;
                });
        },
        toggleSelection: function (deck) {
            deck.selected = !deck.selected;
            this.updateCardCount();
        },
        toggleAll: function () {
            for (deck of this.decks) {
                this.toggleSelection(deck);
            }

            this.updateCardCount();
        },
        selectAll: function () {
            for (deck of this.decks) {
                deck.selected = true;
            }

            this.updateCardCount();
        },
        updateCardCount: function () {
            this.selectedBlack = this.selectedWhite = 0;
            for (deck of this.decks) {
                if (deck.selected) {
                    this.selectedWhite += deck.whiteCount;
                    this.selectedBlack += deck.blackCount;
                }
            }
        },
        getSelectedDeckIds: function () {
            var ids = [];
            for (deck of this.decks) {
                if (deck.selected) {
                    ids.push(deck.id);
                }
            }

            return ids;
        }
    },
    mounted: function () {
        this.getGames();
        this.getDecks();
    }
});