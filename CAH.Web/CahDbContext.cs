﻿using CAH.Web.Entities;
using Microsoft.EntityFrameworkCore;

namespace CAH.Web
{
    public class CahDbContext : DbContext
    {
        public CahDbContext(DbContextOptions<CahDbContext> options) : base(options) { }

        public DbSet<DeckEntity> Decks { get; set; }
    }
}
