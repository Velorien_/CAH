﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Net.WebSockets;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using CAH.Web.Services;
using System.Linq;
using System.Text;

namespace CAH.Web
{
    public class Startup
    {
        private IHostingEnvironment _hostingEnvironment;

        public Startup(IHostingEnvironment env)
        {
            _hostingEnvironment = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddDbContext<CahDbContext>(o => o.UseSqlite("Filename=./cah.db"));
            services.AddSingleton(_hostingEnvironment.ContentRootFileProvider);
            services.AddScoped<DeckService>();
            services.AddSingleton<GameService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseWebSockets();
            app.UseStaticFiles();

            using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            using (var ctx = scope.ServiceProvider.GetService<CahDbContext>())
            {
                ctx.Database.EnsureCreated();
            }

            app.Use(async (http, next) =>
            {
                if (http.WebSockets.IsWebSocketRequest)
                {
                    string id = null;
                    var gameService = http.RequestServices.GetRequiredService<GameService>();

                    try
                    {
                        var webSocket = await http.WebSockets.AcceptWebSocketAsync();
                        id = gameService.AddConnection(webSocket);
                        byte[] buffer;

                        while (webSocket.State == WebSocketState.Open)
                        {
                            buffer = new byte[4096];
                            var result = await webSocket.ReceiveAsync(
                                new ArraySegment<byte>(buffer),
                                CancellationToken.None);

                            string message = Encoding.UTF8.GetString(buffer);
                            switch (result.MessageType)
                            {
                                case WebSocketMessageType.Text:
                                    await gameService.HandleMessage(message, id);
                                    break;
                                case WebSocketMessageType.Close:
                                    gameService.RemoveConnection(id);
                                    await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", CancellationToken.None);
                                    break;
                            }
                        }
                    }
                    catch (WebSocketException)
                    {
                        if(id != null) gameService.RemoveConnection(id);
                    }
                }
                else await next();
            });
        }
    }
}
