﻿using CAH.GameLogic.Models;
using System.Collections.Generic;

namespace CAH.Web.TransferObjects
{
    public class GameDescriptor
    {
        public IEnumerable<int> DeckIds { get; set; }

        public int HandSize { get; set; }

        public int MaxPoints { get; set; }

        public int MinPlayers { get; set; }

        public string GameName { get; set; }

        public bool AllowDiscarding { get; set; }

        public bool IsHidden { get; set; }

        public Ruleset GetRuleset() => new Ruleset(HandSize, MaxPoints, MinPlayers, AllowDiscarding);
    }
}
