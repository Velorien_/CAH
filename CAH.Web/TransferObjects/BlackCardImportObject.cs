﻿namespace CAH.Web.TransferObjects
{
    public class BlackCardImportObject
    {
        public string Text { get; set; }

        public byte Pick { get; set; }
    }
}
