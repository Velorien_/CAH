﻿namespace CAH.Web.TransferObjects
{
    public class DeckInfo
    {
        public DeckInfo(int id, string name, int whiteCount, int blackCount)
        {
            Id = id;
            Name = name;
            WhiteCount = whiteCount;
            BlackCount = blackCount;
        }

        public int Id { get; }

        public string Name { get; }

        public int WhiteCount { get; }

        public int BlackCount { get; }
    }
}
