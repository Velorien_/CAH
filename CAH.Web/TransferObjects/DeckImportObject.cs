﻿using System.Collections.Generic;

namespace CAH.Web.TransferObjects
{
    public class DeckImportObject
    {
        public string Name { get; set; }

        public IEnumerable<string> WhiteCards { get; set; }

        public IEnumerable<BlackCardImportObject> BlackCards { get; set; }
    }
}
